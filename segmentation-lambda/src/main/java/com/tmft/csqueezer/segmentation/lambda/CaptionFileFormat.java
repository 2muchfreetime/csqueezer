package com.tmft.csqueezer.segmentation.lambda;

public enum CaptionFileFormat {
  SCC(".scc"),
  SRT(".srt"),
  VTT(".vtt");

  private final String fileExtension;

  CaptionFileFormat(final String fileExtension) {
    this.fileExtension = fileExtension;
  }

  public String getFileExtension() {
    return fileExtension;
  }
}
