package com.tmft.csqueezer.segmentation.lambda;

import com.tmft.csqueezer.segmentation.lambda.CaptionFileSegmentationTaskOutput.SegmentedCaptionFile;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

final class CaptionFileSegmenter extends CommandExecutor {

  CaptionFileSegmentationTaskOutput executeSegmentation(
      final Locale language,
      final short segmentDurationInSeconds,
      final byte maxLinesPerCaption,
      final byte maxCharactersPerCaption,
      final CaptionFileFormat outputFormat,
      final CaptionFileFormat inputFormat,
      final Path originalCaptionFilePath,
      final Path outputDirectoryPath) throws IOException, InterruptedException {
    final var command = buildCommand(language,
                                     segmentDurationInSeconds,
                                     maxLinesPerCaption,
                                     maxCharactersPerCaption,
                                     outputFormat,
                                     inputFormat,
                                     originalCaptionFilePath,
                                     outputDirectoryPath);
    execute(command);
    final var segmentedCaptionFiles = getSegmentedCaptionFiles(outputDirectoryPath);
    return new CaptionFileSegmentationTaskOutput(outputDirectoryPath, segmentedCaptionFiles);
  }

  private static List<String> buildCommand(
      final Locale language,
      final short segmentDurationInSeconds,
      final byte maxLinesPerCaption,
      final byte maxCharactersPerCaption,
      final CaptionFileFormat outputFormat,
      final CaptionFileFormat inputFormat,
      final Path originalCaptionFilePath,
      final Path outputDirectoryPath) {
    final var segmentedOutputPathPattern =
        outputDirectoryPath.resolve("%d" + outputFormat.getFileExtension())
                           .toString();
    return Stream.of("csegmenter",
                     language.getISO3Language(),
                     segmentDurationInSeconds,
                     maxLinesPerCaption,
                     maxCharactersPerCaption,
                     segmentedOutputPathPattern,
                     outputFormat,
                     inputFormat,
                     originalCaptionFilePath)
                 .map(String::valueOf)
                 .collect(Collectors.toUnmodifiableList());
  }

  private static Set<SegmentedCaptionFile> getSegmentedCaptionFiles(
      final Path outputDirectoryPath) throws IOException {
    return Files.list(outputDirectoryPath)
                .map(Path::toFile)
                .map(SegmentedCaptionFile::new)
                .collect(Collectors.toUnmodifiableSet());
  }
}
