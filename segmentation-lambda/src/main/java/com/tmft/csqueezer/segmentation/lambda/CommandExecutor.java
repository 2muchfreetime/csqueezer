package com.tmft.csqueezer.segmentation.lambda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class CommandExecutor {
  protected final Logger logger = LogManager.getLogger(getClass());

  protected void execute(final List<String> command) throws IOException, InterruptedException {
    final var commandStr = String.join(" ", command);
    final var processBuilder = new ProcessBuilder(command);
    logger.debug("Executing '{}'.", commandStr);
    final var process = processBuilder.start();
    if (0 != process.waitFor()) {
      logError(commandStr, process.getErrorStream());
    }
  }

  private void logError(final String commandStr, final InputStream errorStream) throws IOException {
    try (final var is = new InputStreamReader(errorStream);
         final var br = new BufferedReader(is)) {
      logger.error("Command '{}' failed. Error: {}.", commandStr, br.readLine());
    }
  }
}
