package com.tmft.csqueezer.segmentation.lambda;

import java.io.File;
import java.nio.file.Path;
import java.util.Set;

final class CaptionFileSegmentationTaskOutput {
  private final File parentDirectoryFile;
  private final Set<SegmentedCaptionFile> segmentedCaptionFiles;

  CaptionFileSegmentationTaskOutput(
      final Path parentDirectoryPath,
      final Set<SegmentedCaptionFile> segmentedCaptionFiles) {
    this.parentDirectoryFile = parentDirectoryPath.toFile();
    this.segmentedCaptionFiles = Set.copyOf(segmentedCaptionFiles);
  }

  File getParentDirectoryFile() {
    return parentDirectoryFile;
  }

  Set<SegmentedCaptionFile> getSegmentedCaptionFiles() {
    return segmentedCaptionFiles;
  }

  static final class SegmentedCaptionFile {
    private final int index;
    private final File file;

    SegmentedCaptionFile(final File file) {
      this.index = extractIndex(file);
      this.file = file;
    }

    int getIndex() {
      return index;
    }

    File getFile() {
      return file;
    }

    private static int extractIndex(final File file) {
      final var fileName = file.getName();
      final var lastDotIndex = fileName.lastIndexOf(".");
      final var segmentIndex = fileName.substring(0, lastDotIndex);
      return Integer.parseInt(segmentIndex);
    }
  }
}
