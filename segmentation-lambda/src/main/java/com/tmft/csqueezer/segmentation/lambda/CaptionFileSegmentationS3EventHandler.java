package org.tmft.csqueezer.segmentation.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

public final class CaptionFileSegmentationS3EventHandler extends S3EventHandler {
  private final AmazonS3 amazonS3;
  private final ObjectMapper objectMapper;
  private final CaptionFileSegmenter segmenter;

  public CaptionFileSegmentationS3EventHandler(
      final AmazonS3 amazonS3,
      final ObjectMapper objectMapper,
      final CaptionFileSegmenter segmenter) {
    this.amazonS3 = amazonS3;
    this.objectMapper = objectMapper;
    this.segmenter = segmenter;
  }

  @Override
  public String handleRequest(final S3Event s3event, final Context context) {
    Path workingDirectoryPath = null;
    try {
      workingDirectoryPath = Files.createTempDirectory(null);
      final var objectMetadata = download(s3event);
      final var taskInput = convert(objectMetadata);
      final var taskOutput = process(workingDirectoryPath, taskInput);
    } catch (Exception e) {
      logger.error("", e);
      throw new RuntimeException(e);
    } finally {
      quietlyDeletePath(workingDirectoryPath);
    }
    return "OK";
  }

  private ObjectMetadata download(final S3Event s3event) {
    final var s3Entity = getS3Entity(s3event);
    final var request = createGetObjectRequest(s3Entity);
    logger.info("Starting download of '{}'.", request.getKey());
    final var objectMetadata = amazonS3.getObject(request, null);
    logger.info("Finished download of '{}'.", request.getKey());
    return objectMetadata;
  }

  private CaptionFileSegmentationTaskOutput process(
      final Path workingDirectoryPath,
      final CaptionFileSegmentationTaskInput taskInput) throws IOException, InterruptedException {
    return segmenter.executeSegmentation(taskInput.getSourceCaptionFileLanguage(),
                                         taskInput.getSegmentDurationInSeconds(),
                                         taskInput.getMaxLinesPerCaption(),
                                         taskInput.getMaxCharactersPerCaption(),
                                         taskInput.getOutputFormat(),
                                         taskInput.getInputFormat(),
                                         taskInput.getSourceCaptionFilePath(),
                                         workingDirectoryPath);
  }

  private CaptionFileSegmentationTaskInput convert(final ObjectMetadata objectMetadata) {
    final var cSqueezerMetadata =
        Optional.ofNullable(objectMetadata.getUserMetaDataOf("cSqueezerMetadata"))
                .orElseThrow(() -> new RuntimeException());
    try {
      return objectMapper.readValue(cSqueezerMetadata, CaptionFileSegmentationTaskInput.class);
    } catch (Exception e) {
      logger.error("", e);
      throw new RuntimeException();
    }
  }
}
