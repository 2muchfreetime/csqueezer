package com.tmft.csqueezer.segmentation.lambda;

import java.nio.file.Path;
import java.util.Locale;

final class CaptionFileSegmentationTaskInput {
  private final short segmentDurationInSeconds;
  private final byte maxLinesPerCaption;
  private final byte maxCharactersPerCaption;
  private final CaptionFileFormat outputFormat;
  private final CaptionFileFormat inputFormat;
  private final Locale sourceCaptionFileLanguage;
  private final Path sourceCaptionFilePath;

  CaptionFileSegmentationTaskInput(
      final short segmentDurationInSeconds,
      final byte maxLinesPerCaption,
      final byte maxCharactersPerCaption,
      final CaptionFileFormat outputFormat,
      final CaptionFileFormat inputFormat,
      final Locale sourceCaptionFileLanguage,
      final Path sourceCaptionFilePath) {
    this.segmentDurationInSeconds = segmentDurationInSeconds;
    this.maxLinesPerCaption = maxLinesPerCaption;
    this.maxCharactersPerCaption = maxCharactersPerCaption;
    this.outputFormat = outputFormat;
    this.inputFormat = inputFormat;
    this.sourceCaptionFileLanguage = sourceCaptionFileLanguage;
    this.sourceCaptionFilePath = sourceCaptionFilePath;
  }

  short getSegmentDurationInSeconds() {
    return segmentDurationInSeconds;
  }

  byte getMaxLinesPerCaption() {
    return maxLinesPerCaption;
  }

  byte getMaxCharactersPerCaption() {
    return maxCharactersPerCaption;
  }

  CaptionFileFormat getOutputFormat() {
    return outputFormat;
  }

  CaptionFileFormat getInputFormat() {
    return inputFormat;
  }

  Locale getSourceCaptionFileLanguage() {
    return sourceCaptionFileLanguage;
  }

  Path getSourceCaptionFilePath() {
    return sourceCaptionFilePath;
  }
}
