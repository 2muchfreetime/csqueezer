package com.tmft.csqueezer.segmentation.lambda;

import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.event.S3EventNotification.S3Entity;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.model.GetObjectRequest;
import java.nio.file.Files;
import java.nio.file.Path;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

abstract class S3EventHandler implements RequestHandler<S3Event, String> {
  protected final Logger logger = LogManager.getLogger(getClass());

  protected static S3Entity getS3Entity(final S3Event s3event) {
    return s3event.getRecords()
                  .stream()
                  .findFirst()
                  .map(S3EventNotificationRecord::getS3)
                  .orElseThrow();
  }

  protected static GetObjectRequest createGetObjectRequest(final S3Entity s3Entity) {
    final var bucket = s3Entity.getBucket().getName();
    final var key = s3Entity.getObject().getKey();
    return new GetObjectRequest(bucket, key);
  }

  protected void quietlyDeletePath(final Path path) {
    if (null != path) {
      try {
        if (Files.isDirectory(path)) {
          Files.list(path)
               .forEach(this::quietlyDeletePath);
        } else {
          Files.delete(path);
        }
      } catch (Exception e) {
        logger.warn("Deletion of '{}' failed.", path, e);
      }
    }
  }
}
